<p style="text-align:center!important">
<img src="https://pngimg.com/uploads/bicycle/bicycle_PNG5374.png" width="80px">
</p>
<h1 style="text-align:center!important">Velibs Itinerary</h1>

<div style="text-align:center!important">

</div>

---

<h2 id="setup-le-projet">⚙️ Setup le projet</h2>

Toutes les informations complémentaires sont disponibles dans le repository dédié. En ce qui concerne la configuration globale du projet, nous avons déployé l'API d'authentification et l'API PDF sur deux machines distantes avec docker afin de simplifier l'installation et la rendre moins contraignante.
Il suffit donc d'installer la partie cliente. Il est également important de ne pas modifier les adresses IP du fichier .env.sample du front-end, car elles pointent vers les machines distantes. Une documentation d'installation a été fournie au cas où.

Nous tenons à préciser que certaines fonctionnalités/routes ont été modifiées en prenant en compte votre accord.
<h2 id="participants">👨‍👦‍👦 Participants</h2>

**Mickaël Lebas**, **Alexis Py**, **Théo Posty**
